import React, {Component} from "react";
import './login.less'
import logo from './images/logo.jpg'
import {Form,Input,Button,Checkbox} from "antd";
import { UserOutlined, LockOutlined } from '@ant-design/icons';

export default class Login extends Component{
    handleSubmit=(event)=>{
        event.preventDefault();
    //对密码自定义验证
    //     validatePwd=(rule,value,callback)=>{
    //         if(!value){
    //             callback('密码必须输入!');
    //         }else if(value.length<4){
    //             callback('密码不能小于4位');
    //         }else if(value.length>12){
    //             callback('密码不能大于12位');
    //         }else if(/^[a-zA-Z0-9_]+$/.test(value)){
    //             callback('密码必须是英文、数字、或下划线组成');
    //         }else
    //         {
    //             callback();//success
    //         }
    //     };
    }
    render() {
        return (
            <div className="login">
                <header className="login-header">
                <img src={logo} alt="logo"/>
                    <h1>React项目:后台管理系统</h1>
                </header>
                <section className="login-content">
                    <h2>用户登录</h2>
                    <Form
                        onSubmit={this.handleSubmit}
                        name="normal_login"
                        className="login-form"
                        initialValues={{ remember: true }}

                    >
                        <Form.Item
                            name="username"
                            rules={[{ required: true,whitespace:true, message: '请输入你的用户名!' },
                                {min:4,message:'用户名至少是4位'},
                                {max:12,message:'用户名最多是12位'},
                                {pattern:/^[a-zA-Z0-9_]+$/,message:'用户名必须是英文、数字、或下划线组成'},]}
                        >
                            <Input prefix={<UserOutlined  className="site-form-item-icon"  />}
                                   placeholder="用户名" />
                        </Form.Item>
                        <Form.Item
                            name="password"
                            rules={[{ required: true, message: '请输入你的用户密码!' },
                                {min:4,message:'密码至少是4位'},
                                {max:12,message:'密码最多是16位'},
                                {pattern:/^[a-zA-Z0-9_]+$/,message:'密码必须是英文、数字、或下划线组成'},]}
                        >
                            <Input
                                prefix={<LockOutlined className="site-form-item-icon" />}
                                type="password"
                                placeholder="密码"
                            />
                        </Form.Item>
                        <Form.Item>
                            <Form.Item name="remember" valuePropName="checked" noStyle>
                                <Checkbox>记住我</Checkbox>
                            </Form.Item>

                            <a className="login-form-forgot" href="">
                                忘记密码？
                            </a>
                        </Form.Item>

                        <Form.Item>
                            <Button type="primary" htmlType="submit" className="login-form-button">
                                登录
                            </Button>
                            或者 <a href="">注册</a>
                        </Form.Item>
                    </Form>
                </section>
            </div>
        )
    }

}

// 前台表单验证
